import PySimpleGUI as sg
from os.path import getsize, splitext
import utils as u
import traceback

print("Ignore this terminal, it needs to be here for GUI to compile without")
print("being flagged as malware. MICROSOFT.")

config = u.getconfig()
keypath = config["SSH"]["private_key"]
known_robots = [u.Robot(name, addr) for name, addr in dict(config["ROBOTS"]).items()]

sg.theme("GreenTan")

valid_ext = [("Excel", ".xlsx"), ("Excel (old)",".xls"), ("All", "*")]
show_preview = False

protocols = [
    u.Protocol("Dissolution"),
    u.Protocol("Distribution", meta_default={"length": 24}),
    u.Protocol("Reformat"),
    u.Protocol("TLC Spotting"),
    u.Protocol("48 to Matrix", meta_default={"indexing_for_src": True}),
    u.Protocol("Matrix Transfer", meta_default={"volume": 200}),
    u.Protocol("TLC Matrix", meta_default={"test_spot": True, "columns": 6}),
    u.Protocol("TLC Deepwell", meta_default={"test_spot": True, "columns": 6}),
]


def main_window():
    frame_upper = [
        [sg.Text(size=(90, 10), key="sheet_output")],
    ]

    frame_lower = [
        [sg.Text("", key="retcode")],
    ]

    sheet_options = [
        sg.Text("Sheet Name/Index:"),
        sg.Input("0", size=5, key="-SHEET-"),
        sg.Text("Position Column:"),
        sg.Input("0", size=4, key="pos_index"),
        sg.Text("Volume Column:"),
        sg.Input("1", size=4, key="vol_index"),
        sg.Text("Position Offset:"),
        sg.Input("0", size=4, key="pos_offset")
    ]

    layout = [
        [sg.Text("Choose a protocol."), sg.Combo(protocols, default_value=None, key="protocol_input", readonly=True, enable_events=True)],
        [sg.FileBrowse(target="filename", file_types=valid_ext, initial_folder="/home/spyveej/OTSHEETS"), sg.Input("Please select a spreadsheet.", key="filename", enable_events=True)],
        sheet_options,
        [sg.Text("Metadata (JSON)"), sg.Input(key="-META-")],
        [sg.Button("Apply", key="resubmit")],
        [sg.Frame("Command Preview", frame_upper)],
        [sg.Text("Robot address:"), sg.Combo(known_robots, key="ip_input", enable_events=True), sg.Text("", key="robot_name")],
        [sg.Button("Send commands to robot", key="-NETSEND-", disabled=True)],
        [sg.Frame("OT-2 Response", frame_lower)],
        #[sg.Button("Boot remote script", key="kickstart")],
    ]

    col_left = sg.Column(layout, vertical_alignment='top')
    col_right = sg.Column([
        [sg.Frame("Preview Layout", layout=[[sg.Image(key="-IMG-")]])],
    ], vertical_alignment="middle")

    full_layout = [
        [sg.VPush()],
        [col_left, col_right],
        [sg.VPush()],
    ]
    
    return sg.Window("RexChem Protocol Sender", full_layout)# size=(800, 500))

def preview_popup():
    layout = [
        [sg.Image(key="popimg")],
    ]

    return sg.Window("Preview Layout", layout)


window = main_window()

while True:
    try:
        event, values = window.read()
        if event == sg.WIN_CLOSED or event == "Close":
            break
        if event == "protocol_input":
            # Show layout image when protocol is selected
            picdata = u.getimage(values["protocol_input"].name)
            window["-IMG-"].update(data=picdata)

            # Populate meta input
            window["-META-"].update(values["protocol_input"].meta_default)

        # Get spreadsheet data
        if event == "resubmit":
            filename = values["filename"]
            _, ext = splitext(filename)
            
            # Disqualification checklist
            if values["protocol_input"].meta_required and not any(values["-META-"]):
                sg.Popup(f"The {values["protocol_input"]} protocol requires metadata: {values["protocol_input"].meta_default}")
                window["-NETSEND-"].update(disabled=True)
                continue

            commands = u.commandgrab(values["protocol_input"].name, filename, values["-SHEET-"], \
                values["pos_index"], values["vol_index"], pos_offset=values["pos_offset"], meta=values["-META-"])
            window["sheet_output"].update(commands)
            window["-NETSEND-"].update(disabled=False)
        # Send data to robot
        if event == "-NETSEND-":
            retcode = u.ot_send(commands, values["ip_input"].addr, 65432) # strips ip from formatted robot name
            window["retcode"].update(str(retcode))
        if event == "kickstart":
            print("BUTTON NOT IMPLEMENTED")
            #window.perform_long_operation(u.boot_remote(values["ip_input"].addr, keypath)
    # Error handling block
    except OSError as e:
        window["retcode"].update(f"Could not reach {values['ip_input']}.\nMachine may not be listening or address is invalid.")
    except ConnectionRefusedError:
        window["retcode"].update(f"Target machine {values['ip_input']} refused connection.")
    except Exception as e:
        tb = traceback.format_exc()
        sg.popup_error(f'AN EXCEPTION OCCURRED!', e, tb)

window.close()
