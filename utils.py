import pandas as pd
import socket
import json
import os
import string
from PIL import Image, ImageTk
import fabric as fab
import configparser

class Protocol:
    def __init__(self, name, meta_default={}):
        self.name = name
        self.meta_required = any(meta_default)
        self.meta_default = json.dumps(meta_default)
        
    def __str__(self):
        return self.name

class Robot:
    def __init__(self, name, addr):
        self.name = name
        self.addr = addr
    
    def __str__(self):
        return f"{self.name} : {self.addr}"

def ot_send(data, host, port):
    raw = json.dumps(data)
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.settimeout(5)
        s.connect((host, port))
        s.sendall(bytes(raw, encoding="utf-8"))
        retcode = s.recv(1024)
        return retcode

def lettercheck(index_val) -> int:
    try:
        return int(index_val)
    except ValueError: # Likely to be excel column letter
        return string.ascii_lowercase.index(index_val.lower())


def pullexcel(file_name, sheet_name, loc_index=0, vol_index=1, pos_offset=0):
    try:
        # assume index form
        sn = int(sheet_name)
    except ValueError:
        # must be string name instead
        sn = sheet_name
    
    loc_index = lettercheck(loc_index)
    vol_index = lettercheck(vol_index)
    
    try:
        xl_data = pd.read_excel(file_name, sheet_name=sn, engine="openpyxl")
    except ValueError: # invalid sheet name
        return "INVALID SHEET NAME"
    except FileNotFoundError:
        print(f"Sheet [{file_name}] not found, returning empty.")
        return []
        
    headers = xl_data.columns.values.tolist()
    xl_data.dropna(how="any", subset=[headers[loc_index], headers[vol_index]], inplace=True)
    commands = [[item[loc_index], item[vol_index]] for item in xl_data.itertuples(index=False)]
    try:
        for c in commands:
            # single command looks like [location, volume]
            c[0] += int(pos_offset)
    except TypeError: # position column is not an integer
        print("Cannot apply offset to position column, skipping.")
    return commands

def commandgrab(protocol_name, file_name, sheet_name, loc_index=0, vol_index=1, pos_offset=0, meta="{}"):
    sheet_cmds = pullexcel(file_name, sheet_name, loc_index, vol_index, pos_offset)
    if sheet_cmds == []:
        file_name = "NONE"
    tempmeta = json.loads(meta)
    tempmeta["sheetname"] = f"{os.path.basename(file_name)}/{sheet_name}"
    outmeta = json.dumps(tempmeta)
    data = {
        "proto": protocol_name,
        "data": sheet_cmds,
        "meta": outmeta
    }
    return data


def getimage(protocol_name):
    fname = f"resources/images/{protocol_name}.png"
    if not os.path.exists(fname):
        fname = "resources/images/error.png"

    with Image.open(fname) as img:
        data = img.resize((512, 512))
        return ImageTk.PhotoImage(data)

def boot_remote(addr, keypath):
    with fab.Connection(host=addr, user="root", connect_kwargs={"key_filename": keypath}) as remote:
        remote.run("export RUNNING_ON_PI=1 && python /var/lib/jupyter/notebooks/remote.py", pty=True)

def getconfig():
    config = configparser.ConfigParser()
    config.read("config.ini")
    return config










